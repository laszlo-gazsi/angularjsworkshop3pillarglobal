(function(undefined){
	
	angular.module('TaskManagerApp').directive('renderer', ItemRenderer);

	ItemRenderer.$inject = [];

	function ItemRenderer(){
		return {
			transclude: true,
			template: '<div class="bg-info">&nbsp{{task}}<span ng-transclude></span></div>',
			scope: {
				task: '='
			}
		}
	}

	
	
})();