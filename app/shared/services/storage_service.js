(function(undefined)
{
	angular.module('TaskManagerApp').service('StorageService', StorageService);

	StorageService.$inject = [];

	function StorageService(){

		var tasks = [];

		this.getTasks = function(){
			var tasks = JSON.parse(window.localStorage.getItem('tasks'));
			return (tasks = tasks || []);
		}

		this.storeTasks = function(tasksFormController){
			window.localStorage.setItem('tasks', JSON.stringify(tasksFormController));
		}

		return this;
	}

})();