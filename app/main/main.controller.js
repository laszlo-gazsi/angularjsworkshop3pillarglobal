(function(undefined){
	angular.module('TaskManagerApp').
	controller('TaskManagerAppController', TaskManagerAppController);
	
	TaskManagerAppController.$inject = ['$scope', 'StorageService'];
	
	function TaskManagerAppController($scope, StorageService){
			$scope.taskName = "";
			$scope.tasks = StorageService.getTasks();
			
			$scope.addTask = function(){
				if ($scope.taskName && $scope.taskName.length > 0){
					$scope.tasks.push($scope.taskName);
					$scope.taskName = "";
				}

				StorageService.storeTasks($scope.tasks);
			}
			
			$scope.removeTask = function(index){
				$scope.tasks.splice(index, 1);
				StorageService.storeTasks($scope.tasks);
			}
	}
})();